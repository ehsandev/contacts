<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
    
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    
</head>
<body>
<form>
  <div class="form-group">
  <div class="container-fluid">
     <div class="col-md-6">
      <h2>Add New Contact </h2>  
     </div>

     <div class="col-md-5" style="float:right">
          <button type="button" class="btn btn-warning">View Contacts</button>
     </div>
  </div>
     
 </div>

<div class="form-group"> 
    <div class="col-sm-6">
      <input class="form-control" type="text" placeholder="Name">  
    </div>
</div>

<div class="form-group"> 
    <div class="col-sm-6">
    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="E-mail">
    </div>
</div>

<div class="form-group" > 
    <div class="col-sm-6">
        <input class="form-control" type="text" placeholder="Mobile">
    </div>
</div>

<div class="form-group" > 
    <div class="col-sm-6">
        <input class="form-control" type="text" placeholder="Phone Number">
    </div>
</div>

<div class="form-group" > 
    <div class="col-sm-6">
        <input class="form-control" type="text" placeholder="Address">
    </div>
</div>

<div class="form-group" > 
    <div class="btn-group col-sm-6" role="group" aria-label="Third group">
        <a href="#" class="btn btn-default btn-lg" style="border:1px solid lightgray">Back</a>
        <a href="#" class="btn btn-primary btn-lg">Add Contact</a>
    </div> 
</div>

  
    
</form>




</body>
</html>