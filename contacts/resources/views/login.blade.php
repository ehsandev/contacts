<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
    
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    
</head>
<body>
<form>
  
  
<div class="row">
  <div class="col-sm-4"></div>
  <div class="col-sm-8">
  
  
  <div class="form-group">
  <div class="container-fluid">
  
     <div class="col-md-6">
      <h2>Login! </h2>  
     </div>
    
     <div class="col-md-6">
        <label>Please login in your Account </label>  
        <hr>
     </div>
     
  </div>
 </div>

<div class="form-group" > 
    <div class="col-sm-6">
        <input class="form-control" type="text" placeholder="User Name">
    </div>
</div>

<div class="form-group" > 
    <div class="col-sm-6">
        <input class="form-control" type="password" placeholder="Password">
    </div>
</div>

<div class="form-group" > 
    <div class="btn-group col-sm-6" role="group" aria-label="Third group">
        
        <a href="#" class="btn btn-primary btn-lg btn-block">login</a>
    </div> 
    
</div>
<div class="col-sm-6">
    <h6> You dont have account, please <a href="#">Sign up </a></h6>
</div>
  
  </div>
</div> 
  
    
</form>
  


</body>
</html>